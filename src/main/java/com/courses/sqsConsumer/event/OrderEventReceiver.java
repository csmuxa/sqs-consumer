package com.courses.sqsConsumer.event;

import com.courses.sqsConsumer.model.OrderEvent;
import com.courses.sqsConsumer.service.OrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.aws.messaging.listener.SqsMessageDeletionPolicy;
import org.springframework.cloud.aws.messaging.listener.annotation.SqsListener;
import org.springframework.stereotype.Component;

import java.util.Optional;


@Slf4j
@Component
@RequiredArgsConstructor
public class OrderEventReceiver {

    private static final String CREATE_EVENT = "create";

    private final OrderService orderService;

    @SqsListener(value = "${queue.uri}", deletionPolicy = SqsMessageDeletionPolicy.ON_SUCCESS)
    public void receiveObjectMessage(OrderEvent orderEvent) {
        System.out.println(orderEvent);
        Optional.of(orderEvent)
                .filter(event -> CREATE_EVENT.equals(event.getAction()))
                .ifPresent(orderService::save);
    }

}
