package com.courses.sqsConsumer;

import com.courses.sqsConsumer.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class SqsConsumerApplication{

	private final OrderRepository repository;

	public static void main(String[] args) {
		SpringApplication.run(SqsConsumerApplication.class, args);
	}

}
