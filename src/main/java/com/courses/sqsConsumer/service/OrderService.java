package com.courses.sqsConsumer.service;

import com.courses.sqsConsumer.entity.Order;
import com.courses.sqsConsumer.model.OrderEvent;
import com.courses.sqsConsumer.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OrderService {

    private final OrderRepository repository;

    public void save(OrderEvent simpleEvent) {
        Order order = toOrder(simpleEvent);

        repository.save(order);
    }

    private Order toOrder(OrderEvent event) {
        return new Order()
                .setName(event.getOrderName())
                .setStatus(event.getOrderStatus())
                .setDescription(event.getOrderDescription());
    }

}